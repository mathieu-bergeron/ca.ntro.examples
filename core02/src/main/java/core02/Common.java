package core02;

import ca.ntro.core.NtroCore;

public class Common {

	public static void initialize() {

		NtroCore.factory().registerNamedClass(MsgHelloWorld.class);
		NtroCore.factory().registerNamedClass(MsgAck.class);
		
	}

}
