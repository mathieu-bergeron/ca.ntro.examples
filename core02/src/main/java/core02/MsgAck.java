package core02;

import ca.ntro.core.values.Message;

public class MsgAck extends Message {
	
	private MsgHelloWorld msgHelloWorld;

	public MsgHelloWorld getMsgHelloWorld() {
		return msgHelloWorld;
	}

	public void setMsgHelloWorld(MsgHelloWorld msgHelloWorld) {
		this.msgHelloWorld = msgHelloWorld;
	}

}
