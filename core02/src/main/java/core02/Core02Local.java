package core02;

import ca.ntro.core.NtroCore;
import ca.ntro.core.option.LocalOptions;
import ca.ntro.core.tasks.CoreTasks;
import ca.ntro.core_jdk.NtroCoreApp;

public class Core02Local {
	
	public static void main(String[] args) {
		LocalOptions options = LocalOptions.build();
		options.watchFiles(false);

		NtroCoreApp.launch(); 
	}



}
