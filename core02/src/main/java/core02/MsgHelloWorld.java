package core02;

import ca.ntro.core.values.Message;

public class MsgHelloWorld extends Message {
	
	private String text = null;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
