package core02;

import ca.ntro.core.NtroCore;

public class ClientTasks {

	public static void createTasks() {
		NtroCore.tasks()
		        .task("sendMsg01")
		        .canSend(MsgHelloWorld.class)
		        .executes(inOut -> {
		        	
		        	MsgHelloWorld msg = inOut.get(MsgHelloWorld.class);
		        	
		        	msg.setText("HelloWorld01");
		        	
		        	msg.send();

		        });

		NtroCore.tasks()
		        .task("sendMsg02")
		        .canSend(MsgHelloWorld.class)
		        .executes(inOut -> {
		        	
		        	MsgHelloWorld msg = inOut.get(MsgHelloWorld.class);
		        	
		        	msg.setText("HelloWorld02");
		        	
		        	msg.send();

		        });

		NtroCore.tasks()
		        .task("ack")

		        .receives(MsgAck.class)

		        .executes(inOut -> {
		        	
		        	MsgAck ack = inOut.get(MsgAck.class);
		        	
		        	//NtroCore.logger().info("ack received for: " + ack.getMsgHelloWorld().getText());

		        });
	}

}
