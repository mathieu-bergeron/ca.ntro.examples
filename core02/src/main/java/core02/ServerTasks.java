package core02;

import ca.ntro.core.NtroCore;

public class ServerTasks {

	public static void createTasks() {
		NtroCore.tasks()

		        .task("receivingMsg")

		        .receives(MsgHelloWorld.class)

		        .canSend(MsgAck.class)

		        .executes(inOut -> {
		        	
		        	MsgHelloWorld msg = inOut.get(MsgHelloWorld.class);
		        	
		        	NtroCore.logger().info(msg.getText());
		        	
		        	MsgAck ack = inOut.get(MsgAck.class);
		        	ack.setMsgHelloWorld(msg);
		        	ack.sendTo(msg.sender());
		        });
	}

}
