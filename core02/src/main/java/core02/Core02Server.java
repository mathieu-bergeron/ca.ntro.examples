package core02;

import ca.ntro.core.NtroCoreServer;
import ca.ntro.core.option.ServerOptions;

public class Core02Server {

	public static void main(String[] args) {
		ServerOptions options = ServerOptions.build();
		options.watchFiles(false);

		NtroCoreServer.launch(); 
	}


}
