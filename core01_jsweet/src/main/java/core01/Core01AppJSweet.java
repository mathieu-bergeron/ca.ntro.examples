package core01;

import ca.ntro.core_jsweet.NtroCoreAppJSweet;

public class Core01AppJSweet {
	
	public static void main(String[] args) {
		
		NtroCoreAppJSweet.init(options -> {
			
			Options.localOptions(options);
			
		});

		
		NtroCoreAppJSweet.registerNamedClasses(registrar -> {
			
			
		});


		NtroCoreAppJSweet.defineTasks(tasks -> {

			ClientTasks.defineTasks(tasks);

		});


		NtroCoreAppJSweet.launch(); 
	}

}
