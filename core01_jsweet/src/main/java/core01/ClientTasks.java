package core01;

import ca.ntro.core.services.Logger;
import ca.ntro.core.tasks.CoreTasks;
import ca.ntro.core.tasks.common.Task;

public class ClientTasks {
	

	public static void defineTasks(CoreTasks tasks) {
		tasks.task("hello")
			 .uses(Logger.class)
		     .executes(inOut -> {
		    	 
		    	 Logger logger = inOut.get(Logger.class);
		    	 Task thisTask = inOut.getTask("hello");
		    	 
		    	 logger.info("Hello world");
		    	 
		    	 
		    	 thisTask.destroyInstances();

		        });
	}

}
