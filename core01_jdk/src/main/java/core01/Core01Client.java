package core01;

import ca.ntro.core.NtroCoreClient;
import ca.ntro.core.NtroCore;
import ca.ntro.core.option.ClientOptions;
import ca.ntro.core.tasks.CoreTasks;

public class Core01Client {

	public static void main(String[] args) {
		ClientOptions options = ClientOptions.build();
		options.watchFiles(false);


		NtroCoreClient.launch(); 
	}

}
