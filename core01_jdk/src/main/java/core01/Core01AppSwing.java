package core01;

import ca.ntro.core_swing.NtroCoreAppSwing;

public class Core01AppSwing {
	
	public static void main(String[] args) {
		
		NtroCoreAppSwing.init(options -> {
			
			Options.localOptions(options);
			
		});

		
		NtroCoreAppSwing.registerNamedClasses(registrar -> {
			
			
		});


		NtroCoreAppSwing.defineTasks(tasks -> {

			ClientTasks.defineTasks(tasks);

		});


		NtroCoreAppSwing.launch(); 
	}

}
