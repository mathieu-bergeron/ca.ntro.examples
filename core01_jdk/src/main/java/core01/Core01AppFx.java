package core01;

import ca.ntro.core_fx.NtroCoreAppFx;

public class Core01AppFx {
	
	public static void main(String[] args) {
		
		NtroCoreAppFx.init(options -> {
			
			Options.localOptions(options);
			
		});

		
		NtroCoreAppFx.registerNamedClasses(registrar -> {
			
			
		});


		NtroCoreAppFx.defineTasks(tasks -> {

			ClientTasks.defineTasks(tasks);

		});


		NtroCoreAppFx.launch(); 
	}

}
