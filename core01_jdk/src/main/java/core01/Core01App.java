package core01;

import ca.ntro.core_jdk.NtroCoreApp;

public class Core01App {
	
	public static void main(String[] args) {
		
		NtroCoreApp.init(options -> {
			
			Options.localOptions(options);
			
		});

		
		NtroCoreApp.registerNamedClasses(registrar -> {
			
			
		});


		NtroCoreApp.defineTasks(tasks -> {

			ClientTasks.defineTasks(tasks);

		});


		NtroCoreApp.launch(); 
	}

}
