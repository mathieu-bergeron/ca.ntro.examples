package app01.frontend.tasks;

import ca.ntro.app.tasks.frontend.FrontendTasks;
import static ca.ntro.app.tasks.frontend.FrontendTasks.*;

import app01.frontend.App01Session;
import ca.ntro.core.NtroCore;
import ca.ntro.core.tasks.common.Task;


public class App01Tasks {

	public static void createTasks(FrontendTasks tasks) {

		Task observeSession = tasks.task("observeSession")

			 .follows("modifySession")
		
		     .observes(session(App01Session.class))
		     
		     .executes(entreeSortie -> {
		    	 
		    	 App01Session session = entreeSortie.get(session(App01Session.class));
		    	 NtroCore.logger().info("sessionId: " + session.id());
		    	 
		     });

		Task modifySession = tasks.task("modifySession")
				
			 .follows("loadSession")
	
		     .canModify(session(App01Session.class))
		     
		     .executes(entreeSortie -> {
		    	 
		    	 App01Session session = entreeSortie.get(session(App01Session.class));
		    	 
		    	 session.setSessionId("session modifiée");

		     });
		

		/* TODO
		
		createSession.cancel();

		
		TaskGroup groupeSession = tasks.taskGroup("Session");
		groupeSession.contains(createSession, modifySession, observeSession);
		
		TaskGroup groupeInit = tasks.taskGroup("Init");

		groupeSession.precedes(groupeInit);
		*/

	}


}
