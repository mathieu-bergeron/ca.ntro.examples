package app01.frontend;

import app01.frontend.tasks.App01Tasks;
import ca.ntro.app.tasks.frontend.FrontendTasks;
import ca.ntro.app_fx.frontend.FrontendFx;
import ca.ntro.app_fx.frontend.ViewRegistrarFx;
import ca.ntro.app_impl.signal.event.EventRegistrar;
import ca.ntro.app.session.SessionRegistrar;

public class App01Frontend implements FrontendFx {

	@Override
	public void registerEvents(EventRegistrar registrar) {
	}

	@Override
	public void registerViews(ViewRegistrarFx registrar) {
	}

	@Override
	public void registerSessionClass(SessionRegistrar registrar) {
		registrar.registerSessionClass(App01Session.class);
	}

	@Override
	public void createTasks(FrontendTasks tasks) {
		App01Tasks.createTasks(tasks);
	}

	@Override
	public void execute() {
		
	}

}
