package app01;

import app01.frontend.App01Frontend;
import ca.ntro.app.models.ModelRegistrar;
import ca.ntro.app.signal.message.command.MessageRegistrar;
import ca.ntro.app_fx.frontend.FrontendRegistrarFx;

public class Common {

	public static void registerModels(ModelRegistrar registrar) {
		
	}

	public static void registerMessages(MessageRegistrar registrar) {
		
	}

	public static void registerFrontend(FrontendRegistrarFx registrar) {
		registrar.registerFrontend(new App01Frontend());
	}

}
