package app01;

import app01.backend.App01LocalBackend;
import ca.ntro.app.AppOptions;
import ca.ntro.app.backend.BackendRegistrar;
import ca.ntro.app.models.ModelRegistrar;
import ca.ntro.app.signal.message.command.MessageRegistrar;
import ca.ntro.app_fx.NtroAppFx;
import ca.ntro.app_fx.frontend.FrontendRegistrarFx;

public class App01Local implements NtroAppFx {
	
	public static void main(String[] args) {
		
		// TODO, call NtroAppFx instead of being called
		NtroAppFx.registerModels(registrar -> {
			
		});
		
		// register messages

		// register frontend
		
		// etc.


		
		AppOptions options = AppOptions.build();

		NtroAppFx.launch(options);
	}

	@Override
	public void registerModels(ModelRegistrar registrar) {
		Common.registerModels(registrar);
	}

	@Override
	public void registerMessages(MessageRegistrar registrar) {
		Common.registerMessages(registrar);
	}

	@Override
	public void registerFrontend(FrontendRegistrarFx registrar) {
		Common.registerFrontend(registrar);
	}

	@Override
	public void registerBackend(BackendRegistrar registrar) {
		registrar.registerBackend(new App01LocalBackend());
		
	}

}
