package core04;

import ca.ntro.core.values.Entity;

public class CompteurDeModif implements Entity {
	
	private int nombreDeModif = 0;

	public void incrementer() {
		nombreDeModif++;
	}

	public boolean siNombreMaxAtteint() {
		return nombreDeModif > 3;
	}

}
