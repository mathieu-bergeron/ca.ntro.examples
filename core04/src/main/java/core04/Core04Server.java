package core04;

import ca.ntro.core.NtroCoreServer;
import ca.ntro.core.option.ServerOptions;
import ca.ntro.core.tasks.CoreTasks;

public class Core04Server {

	public static void main(String[] args) {
		ServerOptions options = ServerOptions.build();

		NtroCoreServer.launch(); 
	}


}
