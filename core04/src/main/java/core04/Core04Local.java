package core04;

import ca.ntro.core_jdk.NtroCoreApp;

public class Core04Local {
	
	public static void main(String[] args) {
		NtroCoreApp.defineTasks(tasks -> {

			ClientTasks.defineTasks(tasks);
			
		});

		NtroCoreApp.launch(); 
	}
}
