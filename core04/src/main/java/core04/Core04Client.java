package core04;

import ca.ntro.core.NtroCoreClient;
import ca.ntro.core.option.ClientOptions;
import ca.ntro.core.tasks.CoreTasks;

public class Core04Client {

	public static void main(String[] args) {
		ClientOptions options = ClientOptions.build();

		NtroCoreClient.launch(); 
	}


}
