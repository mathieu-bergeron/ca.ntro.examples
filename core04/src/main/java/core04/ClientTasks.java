package core04;


import ca.ntro.core.services.ExitService;
import ca.ntro.core.services.Logger;
import ca.ntro.core.tasks.CoreTasks;
import ca.ntro.core.values.message._MsgFileChanged;
import ca.ntro.core.values.message._MsgWatchFile;

public class ClientTasks {
	
	public static void defineTasks(CoreTasks tasks) {
		System.out.println("ClientTasks.defineTasks");
		
		tasks.task("creerCompteur")

		     .canCreate(CompteurDeModif.class)

		     .executes(inOut -> {
		        	
		    	 inOut.put(CompteurDeModif.class, new CompteurDeModif());

			});

		tasks.task("suivreFichier")

		     .follows("creerCompteur")

		     .canSend(_MsgWatchFile.class, "{STORAGE}/test.txt") 

		     .executes(inOut -> {
		    	 
		    	 _MsgWatchFile msg = inOut.get(_MsgWatchFile.class, "{STORAGE}/test.txt");

		    	 msg.send();

		     });
		
		
		tasks.task("reagirFichierModifie")

		     .follows("suivreFichier")

		     .uses(Logger.class)
		     .uses(ExitService.class)

		     .receives(_MsgFileChanged.class, "{STORAGE}/test.txt")

		     .canModify(CompteurDeModif.class)

		     .executes(inOut -> {
		        	
		    	Logger          logger   = inOut.get(Logger.class);
		    	ExitService     exiter   = inOut.get(ExitService.class);

				CompteurDeModif compteur = inOut.get(CompteurDeModif.class);
				
				logger.info("le fichier test.txt a été modifié.");
				
				compteur.incrementer();
				
				if(compteur.siNombreMaxAtteint()) {

					logger.info("Nombre max atteint. On quitte");
					
					exiter.exit();
				}
			});
	}

}
