package core03;

import ca.ntro.core.values.Entity;

public class HelloEntity implements Entity {
	
	private String value = null;
	
	public HelloEntity() {
	}

	public HelloEntity(String value) {
		this.value = value;
	}

}
