package core03;

import ca.ntro.core.values.Message;

public class MsgUnknownHello extends Message {
	
	private String text = null;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
