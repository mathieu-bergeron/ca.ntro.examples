package core03;

import ca.ntro.core.NtroCore;
import ca.ntro.core.tasks.common.Id;

public class ClientTasks {

	public static void createTasks() {
		NtroCore.tasks()
				.task("sendHello", Id.valueOf("helloId"))

				.canSend(MsgHelloWorld.class, Id.captureIdInto("helloId"))     // XXX: id not known until a message is sent
				  															   //      We need to store task descriptors
				                                                               //      and instantiate them when possible
				.executes(inOut -> {

					for(int i = 0; i < 5; i++) {
						String helloId = String.format("%02d", i);

						MsgHelloWorld msg = inOut.get(MsgHelloWorld.class, helloId);

						msg.setText("HelloWorld" + msg.targetId());
						
						// XXX: captures targetId into var helloId according to .sends(..., Id.captureIdInto)
						// XXX: creates an instance (or actual task) as "helloId" now has a value
						msg.send();

						// XXX: can only destroy after variant is created
						NtroCore.tasks().destroyInstances("helloId", msg.targetId());
					}
				});

		NtroCore.tasks()
				.task("reportUnknownHello")

				.receives(MsgUnknownHello.class)

				.executes(inOut -> {

					MsgUnknownHello msgUnknownHello = inOut.get(MsgUnknownHello.class);
					
					NtroCore.logger().info(msgUnknownHello.getText());
				});
	}
}
