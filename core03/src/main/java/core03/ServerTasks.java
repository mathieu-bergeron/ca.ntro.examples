package core03;

import java.util.Random;


import ca.ntro.core.NtroCore;
import ca.ntro.core.services.Logger;
import ca.ntro.core.tasks.common.Id;

public class ServerTasks {

	public static void createTasks() {
		NtroCore.tasks()
				.task("validateHelloId", Id.valueOf("helloId"))             

		        .receives(MsgHelloWorld.class, Id.captureIdInto("helloId"))  // XXX: receiving the message creates the instances below
		        															 //      which are guarded not to execute until this task
		        															 //      task validates the desiredEntityId

		        .canSend(MsgUnknownHello.class)                              // XXX: could sends an error message

				.executes(inOut -> {

					MsgHelloWorld msg             = inOut.get(MsgHelloWorld.class, Id.valueOf("helloId"));
					String        desiredHelloId  = msg.targetId();
		        	
		        	if(!isValidHelloId(desiredHelloId)) {

						MsgUnknownHello msgUnknown = inOut.get(MsgUnknownHello.class);
						msgUnknown.setText("Unknown helloId: " + desiredHelloId);
						msgUnknown.sendTo(msg.sender());
						
						// XXX: destroy all task instances that use the invalid helloId,
						//      including the current task instance
						inOut.getTaskInstances(Id.any(), Id.valueOf("helloId")).destroyInstances();
						
		        	}
		        	
		        	// XXX: the task is now executed (and not deleted), 
		        	//      which unlocks the tasks below
				});
		
		
		NtroCore.tasks()
				.task("createEntity", Id.valueOf("helloId"))             
				
				.follows("validateHelloId", Id.valueOf("helloId"))

				.canCreate(HelloEntity.class, Id.valueOf("helloId"))

				.executes(inOut -> {

					// XXX: tests that "manually" created entities have priority
					//      over entities that are created automatically
					inOut.put(HelloEntity.class, Id.valueOf("helloId"), new HelloEntity("created 'manually'"));

				});


		NtroCore.tasks()
				.task("observeEntity", Id.valueOf("helloId"))             
				
				.uses(Logger.class)
				
				.follows("validateHelloId", Id.valueOf("helloId"))   
				//.requires("createEntity", Id.valueOf("helloId"))

				.observes(HelloEntity.class, Id.valueOf("helloId"))   // XXX: can execute as soon as Id.valueOf("helloId") is resolved
																	  //      observations always fire once on installation

				.executes(inOut -> {
					
					Logger      logger = inOut.get(Logger.class);
					HelloEntity entity = inOut.get(HelloEntity.class, Id.valueOf("helloId"));   // XXX: creates the Entity if it does not exists
					
					logger.info("observing: " + entity);
				});


		NtroCore.tasks()
		        .task("receiveMsg", Id.valueOf("helloId"))             
		        
		        .uses(Logger.class)
		         
		        .follows("createEntity", Id.valueOf("helloId"))

		        .receives(MsgHelloWorld.class, Id.valueOf("helloId"))     

		        .canModify(HelloEntity.class, Id.valueOf("helloId"))             

		        .executes(inOut -> {

		        	Logger        logger   = inOut.get(Logger.class);
					MsgHelloWorld msg      = inOut.get(MsgHelloWorld.class, Id.valueOf("helloId"));
					HelloEntity   entity   = inOut.get(HelloEntity.class, Id.valueOf("helloId"));   // XXX: creates the Entity if it does not exists

					logger.info("modifying: " + entity);
					logger.info(msg.getText());
					
					// XXX: we have finished, remove every task instance matching msg.targetId()
					inOut.getTaskInstances(Id.any(), msg.targetId()).destroyInstances();
		        });
	}

	private static boolean isValidHelloId(String currentId) {
		return new Random().nextInt(100) < 75;
	}

}
